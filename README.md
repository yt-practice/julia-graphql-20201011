# julia で graphql サーバを書いてみるテスト

ほとんど https://github.com/codeneomatrix/Diana.jl/tree/master/samples/genie のコピー  

```
$ git clone git@gitlab.com:yt-practice/julia-graphql-20201011.git
$ cd $_
$ julia
julia> ]
(@v1.5) pkg> activate .
(julia-graphql-20201011) pkg> instantiate
(julia-graphql-20201011) pkg> [backspace か ctrl+C]
julia> exit()
$ bin/repl
julia> up()
```

# 変更履歴

```
pkg> add Genie
julia> using Genie
julia> Genie.newapp("julia-graphql-20201011")
(julia-graphql-20201011) pkg> add Diana JSON
julia> edit("app/resources/graphql/GraphqlController.jl")
julia> edit("routes.jl")
```

---

### メモ

関数は再起動しなくても勝手に新しいのに切り替わる  
定数や変数は変えても更新されないが使ってる関数を更新すると切り替わる  
