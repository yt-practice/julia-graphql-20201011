using Genie.Router
import GraphqlController

route("/") do
  serve_static_file("welcome.html")
end

route("/graphql", GraphqlController.playground)
route("/graphql", method="POST", GraphqlController.graphql)
