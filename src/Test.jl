module Test

using Logging, LoggingExtras

function main()
  Base.eval(Main, :(const UserApp = Test))

  include(joinpath("..", "genie.jl"))

  Base.eval(Main, :(const Genie = Test.Genie))
  Base.eval(Main, :(using Genie))
end; main()

end
